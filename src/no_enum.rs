use std::collections::LinkedList;
use std::fs::File;
use std::io::{stdin, stdout, BufReader, Bytes, Read, Write};
use std::ops::{BitAnd, Not};

struct WordIter(Bytes<BufReader<File>>);

impl Iterator for WordIter {
    type Item = u32;

    fn next(&mut self) -> Option<u32> {
        let mut word = 0;
        word += self.0.next()?.expect("Cannot read byte") as u32;
        word <<= 8;
        word += self.0.next()?.expect("Cannot read byte") as u32;
        word <<= 8;
        word += self.0.next()?.expect("Cannot read byte") as u32;
        word <<= 8;
        word += self.0.next()?.expect("Cannot read byte") as u32;
        Some(word)
    }
}

use std::io::ErrorKind::UnexpectedEof;

pub fn exec(path: &str) {
    let file = File::open(path).expect("Cannot open file");
    let buffer = BufReader::new(file);
    let words = WordIter(buffer.bytes());
    let mut mem: Vec<Vec<u32>> = vec![words.collect()];
    let mut reg: [u32; 8] = [0, 0, 0, 0, 0, 0, 0, 0];
    let mut free_id: LinkedList<usize> = LinkedList::new();
    let mut pc = 0;

    loop {
        let instr = mem[0][pc];
        pc += 1;
        match instr >> 28 {
            0 => {
                if reg[(instr & 0x7) as usize] != 0 {
                    reg[((instr >> 6) & 0x7) as usize] = reg[((instr >> 3) & 0x7) as usize];
                }
            }
            1 => {
                reg[((instr >> 6) & 0x7) as usize] = mem
                    [reg[((instr >> 3) & 0x7) as usize] as usize]
                    [reg[(instr & 0x7) as usize] as usize]
            }

            2 => {
                mem[reg[((instr >> 6) & 0x7) as usize] as usize]
                    [reg[((instr >> 3) & 0x7) as usize] as usize] = reg[(instr & 0x7) as usize]
            }
            3 => {
                reg[((instr >> 6) & 0x7) as usize] = reg[((instr >> 3) & 0x7) as usize]
                    .overflowing_add(reg[(instr & 0x7) as usize])
                    .0
            }
            4 => {
                reg[((instr >> 6) & 0x7) as usize] = reg[((instr >> 3) & 0x7) as usize]
                    .overflowing_mul(reg[(instr & 0x7) as usize])
                    .0
            }
            5 => {
                reg[((instr >> 6) & 0x7) as usize] = reg[((instr >> 3) & 0x7) as usize]
                    .overflowing_div(reg[(instr & 0x7) as usize])
                    .0
            }
            6 => {
                reg[((instr >> 6) & 0x7) as usize] = reg[((instr >> 3) & 0x7) as usize]
                    .bitand(reg[(instr & 0x7) as usize])
                    .not()
            }
            7 => return,
            8 => {
                let vec = vec![0; reg[(instr & 0x7) as usize] as usize];
                if let Some(id) = free_id.pop_front() {
                    reg[((instr >> 3) & 0x7) as usize] = id as u32;
                    mem[id] = vec
                } else {
                    reg[((instr >> 3) & 0x7) as usize] = mem.len() as u32;
                    mem.push(vec)
                }
            }
            9 => {
                mem[reg[(instr & 0x7) as usize] as usize] = vec![];
                free_id.push_front(reg[(instr & 0x7) as usize] as usize)
            }
            10 => {
                write!(stdout(), "{}", reg[(instr & 0x7) as usize] as u8 as char)
                    .expect("Cannot write to stdout...");
            }
            11 => {
                let mut buf = [0];
                reg[(instr & 0x7) as usize] = match stdin().read_exact(&mut buf) {
                    Ok(_) => buf[0],
                    Err(e) if e.kind() == UnexpectedEof => 0xFF,
                    Err(e) => panic!("Error '{:?}' while reading stdin...", e),
                } as u32;
            }
            12 => {
                let tab = reg[((instr >> 3) & 0x7) as usize] as usize;
                if tab != 0 {
                    mem[0] = std::mem::replace(&mut mem[tab], vec![]);
                    mem[tab] = mem[0].clone();
                }
                pc = reg[(instr & 0x7) as usize] as usize
            }
            13 => reg[((instr >> 25) & 0x7) as usize] = instr & 0x1FFFFFF,
            _ => unreachable!(),
        }
    }
}
