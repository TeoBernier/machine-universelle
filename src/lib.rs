pub mod no_enum;

#[derive(Debug, Clone, Copy)]
pub enum OPCode {
    CondMove,
    ArrayIndex,
    ArraySet,
    Add,
    Mul,
    Div,
    NotAnd,
    Stop,
    Alloc,
    Free,
    Output,
    Input,
    LoadExec,
    Load,
}

use OPCode::*;

impl OPCode {
    pub fn from_op(op: u32) -> OPCode {
        match op {
            0 => CondMove,
            1 => ArrayIndex,
            2 => ArraySet,
            3 => Add,
            4 => Mul,
            5 => Div,
            6 => NotAnd,
            7 => Stop,
            8 => Alloc,
            9 => Free,
            10 => Output,
            11 => Input,
            12 => LoadExec,
            13 => Load,
            _ => unreachable!("Unknown op code {}", op),
        }
    }

    pub fn from_word(word: u32) -> OPCode {
        OPCode::from_op(word >> 28)
    }
}

#[derive(Debug, Clone, Copy)]
pub enum Instr {
    BaseCommand {
        op: OPCode,
        a: usize,
        b: usize,
        c: usize,
    },
    LoadCommand {
        r: usize,
        value: u32,
    },
}

use Instr::*;

impl Instr {
    pub fn from_word(word: u32) -> Instr {
        match OPCode::from_word(word) {
            Load => LoadCommand {
                // 28 - 25
                r: (word as usize >> 25) & 0b0000_111,
                // last 25 bits
                value: word & 0x1FFFFFF,
            },
            op => BaseCommand {
                op,
                a: (word as usize >> 6) & 0b111,
                b: (word as usize >> 3) & 0b111,
                c: word as usize & 0b111,
            },
        }
    }
}

use std::fs::File;
use std::io::{stdin, stdout, BufReader, Bytes, Read, Write};
use std::ops::{BitAnd, Not};

struct WordIter(Bytes<BufReader<File>>);

impl Iterator for WordIter {
    type Item = u32;

    fn next(&mut self) -> Option<u32> {
        let mut word = 0;
        word += self.0.next()?.expect("Cannot read byte") as u32;
        word <<= 8;
        word += self.0.next()?.expect("Cannot read byte") as u32;
        word <<= 8;
        word += self.0.next()?.expect("Cannot read byte") as u32;
        word <<= 8;
        word += self.0.next()?.expect("Cannot read byte") as u32;
        Some(word)
    }
}

use std::io::ErrorKind::UnexpectedEof;

pub struct Machine {
    mem: Vec<Vec<u32>>,
    reg: [u32; 8],
    pc: usize,
    free_id: Vec<usize>,
}

impl Machine {
    pub fn new(path: &str) -> Machine {
        let file = File::open(path).expect("Cannot open file");
        let buffer = BufReader::new(file);
        let words = WordIter(buffer.bytes());
        Machine {
            mem: vec![words.collect()],
            reg: [0, 0, 0, 0, 0, 0, 0, 0],
            pc: 0,
            free_id: vec![],
        }
    }

    fn exec_next(&mut self) -> Option<()> {
        let instr = Instr::from_word(self.mem[0][self.pc]);
        self.pc += 1;
        let Machine {
            ref mut mem,
            ref mut reg,
            ref mut free_id,
            ref mut pc,
            ..
        } = self;

        match instr {
            BaseCommand { op, a, b, c } => match op {
                CondMove => {
                    if reg[c] != 0 {
                        reg[a] = reg[b];
                    }
                }
                ArrayIndex => reg[a] = mem[reg[b] as usize][reg[c] as usize],

                ArraySet => mem[reg[a] as usize][reg[b] as usize] = reg[c],
                Add => reg[a] = reg[b].overflowing_add(reg[c]).0,
                Mul => reg[a] = reg[b].overflowing_mul(reg[c]).0,
                Div => reg[a] = reg[b].overflowing_div(reg[c]).0,
                NotAnd => reg[a] = reg[b].bitand(reg[c]).not(),
                Stop => return None,
                Alloc => {
                    let vec = vec![0; reg[c] as usize];
                    if let Some(id) = free_id.pop() {
                        reg[b] = id as u32;
                        mem[id] = vec
                    } else {
                        reg[b] = mem.len() as u32;
                        mem.push(vec)
                    }
                }
                Free => {
                    mem[reg[c] as usize] = vec![];
                    free_id.push(reg[c] as usize)
                }
                Output => {
                    write!(stdout(), "{}", reg[c] as u8 as char)
                        .expect("Cannot write to stdout...");
                }
                Input => {
                    let mut buf = [0];
                    reg[c] = match stdin().read_exact(&mut buf) {
                        Ok(_) => buf[0],
                        Err(e) if e.kind() == UnexpectedEof => 0xFF,
                        Err(e) => panic!("Error '{:?}' while reading stdin...", e),
                    } as u32;
                }
                LoadExec => {
                    let tab = reg[b] as usize;
                    if tab != 0 {
                        mem[0] = mem[tab].clone()
                    }
                    *pc = reg[c] as usize
                }
                _ => unreachable!(),
            },
            LoadCommand { r, value } => reg[r as usize] = value,
        }
        Some(())
    }

    pub fn exec(&mut self) {
        while let Some(_) = self.exec_next() {}
    }
}
